void main()
{
	float2 IntRes = GetResolution();
	float2 InvIntRes = GetInvResolution();
	float pixelSize = round(GetResolution().y / 264.0);
	float2 lowResPosition = floor(GetCoordinates() * IntRes / pixelSize) * pixelSize * InvIntRes;
	float4 c0 = SampleLocation(lowResPosition);
	SetOutput(c0);
}
