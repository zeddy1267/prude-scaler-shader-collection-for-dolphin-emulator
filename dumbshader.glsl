// Nearest neighbor filtering, with optional Sharp Bilinear filtering

/*
[configuration]

[OptionBool]
GUIName = Sharp bilinear filtering on dumbshader
OptionName = USE_BILINEAR
DefaultValue = true

[OptionRangeFloat]
GUIName = Prescale Factor for sharp bilinear. Higher is sharper.
OptionName = zPRESCALE_FACTOR
MinValue = 1.0
MaxValue = 32.0
StepAmount = 1.0
DefaultValue = 16.0
DependentOption = USE_BILINEAR

[/configuration]
*/

void main()
{
	float2 IntRes = GetResolution();
	float2 InvIntRes = GetInvResolution();
	float2 position = (GetCoordinates() * IntRes);
	float scale = GetOption(zPRESCALE_FACTOR);

	float regionRange = 1.0;

	if (OptionEnabled(USE_BILINEAR))
		regionRange = 0.5 - (0.5 / scale);

	float2 s = fract(position);
	float2 centerDist = s - 0.5;
	float2 f = ((centerDist - clamp(centerDist, -regionRange, regionRange)) * scale + 0.5);

	float2 positionFloored = floor(position);
	float2 modPos = positionFloored + f;

	float2 sampleHere = modPos / IntRes;
	float4 color = SampleLocation(sampleHere);
	SetOutput(float4(color));
}
