void main()
{
	float2 IntRes = GetResolution();
	float2 InvIntRes = GetInvResolution();

	float pixelSize = round(GetResolution().y / 264.0);

	float scale = 1.0 / pixelSize;

	float2 pixelOffset = ((InvIntRes * pixelSize  + InvIntRes) / 2.0) / 2.0;

	float2 position = (GetCoordinates() * IntRes) / pixelSize + pixelOffset;

	float regionRange = 0.5 - (0.5 / scale);

	float2 s = fract(position);
	float2 centerDist = s - 0.5;
	float2 f = ((centerDist - clamp(centerDist, -regionRange, regionRange)) * (scale) + 0.5);

	float2 positionFloored = floor(position - InvIntRes * 0.5) * pixelSize;
	float2 modPos = positionFloored + f;

	float2 sampleHere = modPos / IntRes;
	float4 color = SampleLocation(sampleHere + pixelOffset);
	SetOutput(float4(color));
}
