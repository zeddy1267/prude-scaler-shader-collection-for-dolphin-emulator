// Combination of dumbshader and crude_division

// Scanline code stolen from https://gist.github.com/mariodivece/dab278f82ef3725b06001adec28ef92b

/*
[configuration]

[OptionBool]
GUIName = Divide Resolution
OptionName = DIVIDE_RES
DefaultValue = false

[OptionRangeFloat]
GUIName = Division Factor
OptionName = FACTOR
MinValue = 2.0
MaxValue = 8.0
StepAmount = 1.0
DefaultValue = 2.0
DependentOption = DIVIDE_RES

[OptionBool]
GUIName = Target Resolution instead of division factor
OptionName = W_TARGET_RES
DefaultValue = false
DependentOption = DIVIDE_RES

[OptionRangeInteger]
GUIName = Target Resolution
OptionName = X_TARGET_HEIGHT
MinValue = 240
MaxValue = 480
StepAmount = 240
DefaultValue = 240
DependentOption = DIVIDE_RES

[OptionBool]
GUIName = Always correct offset. May induce or reduce blur depending on other configurations
OptionName = e_ALWAYS_OFFSET
DefaultValue = true
DependentOption = DIVIDE_RES

[OptionBool]
GUIName = Correct offset at high division factors, but only on odd values to help with rounding
OptionName = f_USE_OFFSET
DefaultValue = true
DependentOption = DIVIDE_RES


[OptionBool]
GUIName = dumbshader (Nearest Neighbor Scaling)
OptionName = USE_DUMB
DefaultValue = true

[OptionBool]
GUIName = Sharp Bilinear Filtering on dumbshader
OptionName = USE_BILINEAR
DefaultValue = true
DependentOption = USE_DUMB

[OptionBool]
GUIName = Use (1 / Division Factor) as Prescale Factor
OptionName = y_DIVISION_PRESCALE
DefaultValue = true
DependentOption = USE_DUMB

[OptionRangeFloat]
GUIName = Prescale Factor for Sharp Bilinear
OptionName = z_PRESCALE_FACTOR
MinValue = 1.0
MaxValue = 32.0
StepAmount = 1.0
DefaultValue = 16.0
DependentOption = USE_DUMB


[OptionBool]
GUIName = Scanlines
OptionName = Z_ENABLE_SCANLINE
DefaultValue = false

[OptionBool]
GUIName = Horizontal Scanlines
OptionName = A_H_SCANLINE
DefaultValue = true
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Horizontal Scanline Interval
OptionName = A_H_SCANLINE_SIZE
MinValue = 0.0
MaxValue = 16.0
StepAmount = 1.0
DefaultValue = 4.0
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Horizontal Scanline Offset
OptionName = A_H_SCAN_OFFSET
MinValue = 0.0
MaxValue = 8.0
StepAmount = 0.5
DefaultValue = 2.5
DependentOption = Z_ENABLE_SCANLINE

[OptionBool]
GUIName = Vertical Scanlines
OptionName = B_V_SCANLINE
DefaultValue = false
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Vertical Scanline Interval
OptionName = B_V_SCANLINE_SIZE
MinValue = 0.0
MaxValue = 16.0
StepAmount = 1.0
DefaultValue = 4.0
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Vertical Scanline Offset
OptionName = B_V_SCAN_OFFSET
MinValue = 0.0
MaxValue = 8.0
StepAmount = 0.5
DefaultValue = 2.5
DependentOption = Z_ENABLE_SCANLINE

[OptionBool]
GUIName = Use Solid Line
OptionName = a_SOLID
DefaultValue = false
DependentOption = Z_ENABLE_SCANLINE

[OptionBool]
GUIName = Use Window Resolution Instead of Internal Resolution
OptionName = b_WIN_RES
DefaultValue = false
DependentOption = Z_ENABLE_SCANLINE

[OptionBool]
GUIName = Scale Line Thickness with Window Size
OptionName = c_SCALE_RES
DefaultValue = true
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Scanline Opacity
OptionName = d_SCAN_OPACITY
MinValue = 0.0
MaxValue = 1.0
StepAmount = 0.01
DefaultValue = 0.66
DependentOption = Z_ENABLE_SCANLINE

[OptionRangeFloat]
GUIName = Brightness Boost
OptionName = e_BRIGHTNESS_BOOST
MinValue = 0.0
MaxValue = 2.0
StepAmount = 0.05
DefaultValue = 1.0
DependentOption = Z_ENABLE_SCANLINE

[/configuration]
*/
// General Variables
	float2 intRes = GetResolution();
	float2 winRes = GetWindowResolution();
	float2 invIntRes = GetInvResolution();
	float pixelSize = GetOption(FACTOR);
	float2 pixelOffset = float2(0.0, 0.0);
	const float PI = 3.1415926535897932384626433832795;



float CalculateScanlines() {

	if (!OptionEnabled(Z_ENABLE_SCANLINE))
		return 1.0; // Equivalent to no scanlines whatsoever

	// General Scanline Variables
	float scanlineOpacity = 1.0 - GetOption(d_SCAN_OPACITY);
	float lightBoost = GetOption(e_BRIGHTNESS_BOOST);

	// Bases the scanline's position off the internal resolution by default. This keeps it aligned with the pixels.
	float2 scanlineRes = intRes;

	float windowSize = round(winRes.x / 640.0);

	// Scales the scanlines based on the window size, while still keeping them pixel aligned
	if (OptionEnabled(c_SCALE_RES))
		scanlineRes = windowSize * intRes;

	// Sets the scanline interval to be purely based on the size of the window
	if (OptionEnabled(b_WIN_RES))
		scanlineRes = winRes;

	// Horizontal Scanline Variables
	float horizontalScanlineSize = GetOption(A_H_SCANLINE_SIZE);

	// Sets it arbitrarily high, this is equivalent to not existing.
	if (!OptionEnabled(A_H_SCANLINE))
		horizontalScanlineSize = 1000000.0;


	float horizontalScanlineOffset = GetOption(A_H_SCAN_OFFSET);

	float vPos = (GetCoordinates().y) * (scanlineRes.y) - horizontalScanlineOffset;

	// get color factor intensity based on vertical pixel position
	float horizontalLineIntensity = abs(cos((PI / horizontalScanlineSize) * vPos));

	float horizontalScanlineColor = clamp(horizontalLineIntensity, 0.0, 1.125);

	// Sets the scanline to be a solid line
	if (OptionEnabled(a_SOLID))
		horizontalScanlineColor = round(clamp(horizontalLineIntensity, 0.0, 1.0));

	float c1 = horizontalScanlineColor;

	// Vertical Scanline Variables
	float verticalScanlineSize = GetOption(B_V_SCANLINE_SIZE);

	if (!OptionEnabled(B_V_SCANLINE))
		verticalScanlineSize = 1000000.0;

	float verticalScanlineOffset = GetOption(B_V_SCAN_OFFSET);

	float hPos = (GetCoordinates().x) * (scanlineRes.x) - verticalScanlineOffset;

	float verticalLineIntensity = abs(cos((PI / verticalScanlineSize) * hPos));

	float verticalScanlineColor = clamp(verticalLineIntensity, 0.0, 1.125);

	if (OptionEnabled(a_SOLID))
		verticalScanlineColor = round(clamp(verticalLineIntensity, 0.0, 1.0));

	float c2 = verticalScanlineColor;


	float c3 = (c1 * c2);
	c3 += (1.0 - c3) * scanlineOpacity;
	c3 *= lightBoost;
	return c3;
}

void main()
{

	float scanlineColour = CalculateScanlines();

	float targetHeight = 0.0;

	if (GetOption(X_TARGET_HEIGHT) == 240)
		targetHeight = 264.0;

	if (GetOption(X_TARGET_HEIGHT) == 480)
		targetHeight = 528.0;

	if (!OptionEnabled(DIVIDE_RES))
		pixelSize = 1.0;

	if (OptionEnabled(DIVIDE_RES)) {

		if (OptionEnabled(W_TARGET_RES))
			pixelSize = round(intRes.y / targetHeight);

		if (OptionEnabled(e_ALWAYS_OFFSET))
			pixelOffset = (invIntRes * pixelSize + invIntRes) / 4.0;

		else if (OptionEnabled(f_USE_OFFSET)) {
			if (mod(pixelSize, 2.0) == 1.0) {

				if (pixelSize > 1.0)
					pixelOffset = (invIntRes * pixelSize + invIntRes) / 4.0;
			}
		}
	}

	if (OptionEnabled(USE_DUMB)) {

		float2 position = (GetCoordinates() * intRes) / pixelSize + pixelOffset;

		float scale = GetOption(z_PRESCALE_FACTOR);

		if (OptionEnabled(y_DIVISION_PRESCALE)) {

			if (OptionEnabled(DIVIDE_RES))
				scale = 1.0 / pixelSize;
		}

		float regionRange = 1.0;

		if (OptionEnabled(USE_BILINEAR))
			regionRange = 0.5 - (0.5 / scale);

		float2 s = fract(position);
		float2 centerDist = s - 0.5;
		float2 f = ((centerDist - clamp(centerDist, -regionRange, regionRange)) * (scale) + 0.5);

		float2 positionFloored = floor(position - invIntRes * 0.5) * pixelSize;
		float2 modPos = positionFloored + f;

		float2 sampleHere = modPos / intRes;
		float4 c0 = SampleLocation(sampleHere + pixelOffset);

		SetOutput(c0 * scanlineColour);
	}

	else {

		float2 lowResPosition = floor(GetCoordinates() * intRes / pixelSize) * pixelSize * invIntRes + pixelOffset;
		float4 c0 = SampleLocation(lowResPosition);

		if (pixelSize == 1.0)
			SetOutput(Sample() * scanlineColour);
		else
			SetOutput(c0 * scanlineColour);
	}
}






