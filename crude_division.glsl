// Crudely divides Dolphin's internal resolution

/*
[configuration]

[OptionRangeFloat]
GUIName = Division Factor
OptionName = FACTOR
MinValue = 1.0
MaxValue = 8.0
StepAmount = 1.0
DefaultValue = 2.0

[OptionBool]
GUIName = Target Resolution instead of division factor
OptionName = WTARGET_RES
DefaultValue = false
DependentOption = FACTOR

[OptionRangeInteger]
GUIName = Target Resolution
OptionName = X_TARGET_HEIGHT
MinValue = 240
MaxValue = 480
StepAmount = 240
DefaultValue = 240
DependentOption = FACTOR

[OptionBool]
GUIName = Always correct offset. May induce or reduce blur depending on your setup
OptionName = g_ALWAYS_OFFSET
DefaultValue = false
DependentOption = FACTOR

[OptionBool]
GUIName = Correct offset at high division factors, but only on odd values to help with rounding
OptionName = z_USE_OFFSET
DefaultValue = true
DependentOption = FACTOR

[/configuration]
*/


void main()
{
	float2 IntRes = GetResolution();
	float2 InvIntRes = GetInvResolution();
	float pixelSize = GetOption(FACTOR);
	float targetHeight = 0.0;
	if (GetOption(X_TARGET_HEIGHT) == 240)
		targetHeight = 264.0;
	if (GetOption(X_TARGET_HEIGHT) == 480)
		targetHeight = 528.0;
	float2 pixelOffset = float2(0.0, 0.0);


	if (OptionEnabled(WTARGET_RES))
			pixelSize = ceil(IntRes.y / targetHeight);

	if (OptionEnabled(g_ALWAYS_OFFSET))
		pixelOffset = (InvIntRes * pixelSize + InvIntRes) / 2.0;

	else if (OptionEnabled(z_USE_OFFSET)) {
		if (mod(pixelSize, 2.0) == 1.0) {

			if (pixelSize > 1.0)
				pixelOffset = (InvIntRes * pixelSize + InvIntRes) / 2.0;
		}
	}

	float2 lowResPosition = floor(GetCoordinates() * IntRes / pixelSize) * pixelSize * InvIntRes + pixelOffset;
	float4 c0 = SampleLocation(lowResPosition);

	if (pixelSize == 1.0)
		SetOutput(Sample());
	else
		SetOutput(c0);
}
