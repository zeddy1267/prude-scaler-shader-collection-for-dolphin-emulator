# Prude Scaler Shader Collection for Dolphin Emulator

[Download shaders](https://gitlab.com/zeddy1267/prude-scaler-shader-collection-for-dolphin-emulator/uploads/1cb311b7b530094026a15ec264061918/Shader_Pack.zip)

Hey! Thanks for checking out this shader pack. If you're reading this, you've probably downloaded this to try and get the most out of Animal Crossing visually, however, some of these are nice to have for GCN/Wii games in general!

These shaders are designed to bring out games with your Dolphin Emulator's internal resolution set to "Native", if in case you like more visible, chunky pixels on older games.

All attatched screenshots are with a window size that's x2 the game's native resolution, and Dolphin's internal resolution is set to "Native", as well as texture filtering set to "Force Nearest", unless otherwise specified.

TL;DR the best one for most of your needs will be 'dumbshader.glsl', and 'prude_scalar.glsl' serves as dumbshader with many more features.

## Credits

Big shoutouts to the folks who worked on the Animal Crossing HD pack, as I've included some of their resources in the release of this project. https://forums.dolphin-emu.org/Thread-animal-crossing-hd-texture-pack-october-7th-2018

"mariodivece", whom I stole the scanline code from https://gist.github.com/mariodivece/dab278f82ef3725b06001adec28ef92b

Dolphin Emulator is obviously a huge player in all of this, since these shaders are specifically designed for Dolphin Emulator. https://dolphin-emu.org/

The "Sharp Bilinear" filter, which was modified from https://github.com/dolphin-emu/dolphin/blob/ca0b61be911eda0d8ece746d6ba707b54402cb67/Data/Sys/Shaders/sharp_bilinear.glsl

Most importantly, I'd like to wish an awful day on my "good" friend July, who's screenshot of an attempt at low-res AC was so bad, that I immediately had to drop every project I was working on to spend dozens of hours putting this all together, just to try and make up for the assault she caused to my eyes. This wouldn't have been possible without her.

## Animal Crossing Texture Packs

![use_this_font](https://cdn.discordapp.com/attachments/1102669854617649162/1102706126191075418/image.png)
*custom font, and downscaled HD UI, with dumbshader*

The elements that take the biggest hit from any form of scaling are UI stuff. For primarily 2d games, you should probably be running them with a higher internal resolution, instead of using these shaders.
First off, for Animal Crossing, the element that takes the biggest hit with any form of scaling is the UI, ESPECIALLY the font. That's why there are several versions of the [Animal Crossing HD texture pack](https://forums.dolphin-emu.org/Thread-animal-crossing-hd-texture-pack-october-7th-2018) included with the download in [releases](https://gitlab.com/zeddy1267/prude-scaler-shader-collection-for-dolphin-emulator/-/releases), to be used with [Dolphin Emulator](https://dolphin-emu.org/)

There are 3 versions of the pack included so far, and all of them are merely to help pick out your personal preference, except for the font pack, which is almost required. The included texture packs share the same directory structure as the Animal Crossing HD pack, so they can be written into the HD pack, or into each other. Note that these were only tested with the USA gamecube release of Animal Crossing, and may not work on other versions of the game. Just click which pack you'd like to download!

 - [GAF (font only)](https://gitlab.com/zeddy1267/prude-scaler-shader-collection-for-dolphin-emulator/uploads/c3fb21278911f6a63bfa7677570f12ea/GAF__font_only_.zip)

    This is the most crucial pack of the bunch. It contains the font used in the Animal Crossing HD texture pack, downscaled to 25% its size, with bicubic filtering. I do not recommend playing without this specially scaled font. The font is also included in the git repository, because of how crucial it is.

 - [GAF (UI elements only, no downscale)](https://gitlab.com/zeddy1267/prude-scaler-shader-collection-for-dolphin-emulator/uploads/b717f6d8911ac885ee1fd1497709b05b/GAF__UI_elements_only__no_downscale_.zip)

    This, as the name suggests, is a version of the pack which contains all UI elements from the HD pack, with no downscale applied. This does NOT include the custom font from above, so I strongly recommend replacing the font in this pack with the one from the above.


 - [GAF (Whole HD pack downscaled)](https://gitlab.com/zeddy1267/prude-scaler-shader-collection-for-dolphin-emulator/uploads/aaef964616b5653f0c8c211895372335/GAF__Whole_HD_pack_downscaled_.zip)

   This is the whole HD pack, but converted to PNG, and scaled down to 25% it's original size, with bicubic filtering, just like the font.

Here's a sidequest! Download the full HD pack from the source, and downscale the font to 12.5% (1/8). This will net you a very pixellated font, similar to how the game's font normally looks, but with some better filtering.

![2d_ui_fail](https://cdn.discordapp.com/attachments/1102669854617649162/1102914300697387069/image.png)
*While not Animal Crossing, this displays the general issues caused to the UI in games. This is a quirck of enabling "Forced Nearest" texture filtering, while having internal resolution set to native, which are the settings most these shaders were develoved around. Again, for primarily 2d games, I'd recommend just playing them with a higher internal resolution, without using any of these shaders.*

## Animal Crossing's filtered textures

![filter_off](https://cdn.discordapp.com/attachments/1102669854617649162/1102698012481949767/image.png)
*you can barely see her pretty little face*

![filter_on](https://cdn.discordapp.com/attachments/1102669854617649162/1102698142438277160/image.png)
*There we go! Chunky, but visible. This is using dumbshader with texture filtering set to "Forced Nearest"*

Animal Crossing has an egregious amount of filtering on its textures by default. This can be disabled in Dolphin by going to the graphics settings, going to the "Enhancements" tab, and setting "Texture Filtering" to "Forced Nearest".

Almost all of these shaders were tested with "Forced Nearest", but it's all up to personal preference.


## the PAR conundrum

![PAR+issue](https://cdn.discordapp.com/attachments/1102669854617649162/1102700106354012180/image.png)
*Despite the window being scaled to exactly 2 times the "native resolution", there's some 'pixels' which are 2x3, when they should all be 2x2, indicating that the game is not being scaled up properly*

Before I tackle the shaders, there's a big issue with Gamecube games and their Pixel Aspect Ratio. Every Gamecube game has an internal native resolution. For the US release of Animal Crossing: Population Growing, this internal resolution is 608x468. For whatever reason, Dolphin struggles with scaling the window properly. I'm unsure if this is a quirk of Dolphin, or a quirk of Gamecube games, but achieving 100% 1:1 square pixels is a fool's errand. If you can pull it off, it'll yield the best results from any of these shaders, but it's barely worth it, and the difference it makes is impossible to tell with the human eye, unless you KNOW what to look for. The one exception is the scanline options, which barely work without a 1:1 PAR.

If you would like 1:1 PAR in USA Animal Crossing, It's a bit of a setup, and I'm unsure if every operating system can do it. For a window size about the same as 'x2 Native', in Dolphin you'll need to setup a few requirements.

 - Set the window size to 1216x961. There's no way to do this with Dolphin configurations, so you'll need some sort of external tool to do this, which you'll have to find on your own. This also means that this trick only works in windowed mode.

 - Set Dolphin's aspect ratio to "Stretch to Window"

 - Set interal resolution to "Native"

It may be possible to write a shader that automatically adds a margin around the game, to keep it at a 1:1 PAR resolution, similar to what [this](https://github.com/dolphin-emu/dolphin/blob/ca0b61be911eda0d8ece746d6ba707b54402cb67/Data/Sys/Shaders/integer_scaling.glsl) does, however, that's beyond the scope of this project. If you have any success doing that, please let me know!

## Scaling the Window to an acceptable size in Dolphin

Firstly, a little tip. Dolphin currently has no settings to handle scaling the game by an integer amount, which is annoying. The next best thing is to use Dolphin's "Auto-Adjust Window Size" Option, in the "General" tab of graphics configuration. This will scale the Dolphin window to be perfectly sized to your internal resolution. Unfortunately, with your internal resolution set to "Native", the window will be rather small. A little trick for this is to set Dolphin's internal resolution to "x2 Native (1280x1056) for 720p", with "Auto-Adjust Window Size" on, will scale the window to a more reasonable size. However, it's still not quite an integer scale, as covered by "__the PAR conundrum__", but it's only a couple of pixels off, and will still yield fantastic results. From there, you can disable "Auto-Adjust Window Size", and turn the resolution back down to native. Each game's internal resolution is different, so this only works on a per-game basis.

## Shaders

Anyways, The shaders! Let me go over them. To install, move the contents inside of the included 'Shaders' folder (the '*.glsl' files) into the 'Shaders' folder in your Dolphin's directory. Should be located in documents on Windows, and '.local/share/dolphin-emu/' on Linux. MacOS users, you're on your own!

### dumbshader
![dumbshader_example(1:1PAR)](https://cdn.discordapp.com/attachments/1102669854617649162/1102686514556715118/image.png)
*PAR is 1:1 (window res 1216x961)*

![dumbshader_example2](https://cdn.discordapp.com/attachments/1102669854617649162/1102670866032103474/image.png)

![dumbshader_example3](https://cdn.discordapp.com/attachments/1102669854617649162/1102692495252529223/image.png)


for most uses, dumbshader is the one you're probably looking for, but please check out the more feature-rich varient of it, "__prude_scaler__". With Dolphin internal resolution set to native, dumbshader offers a clean experience with sharp pixels, for any game running at native resolution, regardless of the actual size and scale of the window!

'dumbshader.glsl' has a few settings which can be configured in Dolphin.

 - Sharp Bilinear Filtering on dumbshader

    This option induces a very, very subtle blur on elements in the game. In most cases, this looks a bit better than having it disabled, largely due to the PAR conundrum covered above.

 - Prescale Factor for Sharp Bilinear

    This slider sets how much the shader scales up the resolution before applying a standard bilinear filter. The higher this value is, the closer the image will look to having Sharp Bilinear disabled entirely. This does nothing if Sharp Bilinear isn't turned on in the first place.

### crude_division
![cude_division_example](https://cdn.discordapp.com/attachments/1102669854617649162/1102671344421838940/image.png)
*blurry, but matches the n64 game quite well. Texture filtering is set to default*

The other main shader here is 'crude_division.glsl'. This does as the name suggests, and crudely divides the resolution. Out of the box, it's configured to make Animal Crossing look as similar to the Japanese N64 version, "Doubutsu no Mori" With Dolphin's internal resolution set to native, and texture filtering set to "Default".

 - Division Factor

    The division factor slider sets how much the resolution will be divided, based on your Internal Resolution.

 - Target Resolution instead of division factor

    "Target Resolution instead of division factor" and it's corresponding slider, automatically sets your game's resolution to the set value. This overides the Division Factor option.

 - Correct offset

    There two settings correct the offset of dividing the resolution. Lowering the resolution is more or less equivalent to enlarging the pixel in the top left corner. repeat this for the whole window, and everything gets pushed to the right, and pushed down. By default, the offset is only corrected when the division factor is an odd value, which works pretty well. The Option for always correcting the offset will work great for keeping things in the same place~ but unpredictable sharpens or blurs depending on other settings set in Dolphin.

### prude_scaler (The best one)

The real star of the show here is 'prude_scaler.glsl'. This is a combination of every shader in this pack. With enough fiddling, 'prude_scaler.glsl' can replicate anything else you see, and more! By default, 'prude_scaler.glsl' is set to look the same as 'dumbshader.glsl', which, might I remind, is probably all you want.

 - Divide Resolution
   ![divide_7](https://cdn.discordapp.com/attachments/1102669854617649162/1102696883081719929/image.png)
   *you almost can't tell that I haven't been picking the weeds in my town*

   The option to always correct offset is even less predictable here, as now it's affected by every other option. Sometimes it ends up looking pretty good though, so play around with it!

 - dumbshader (Nearest Neighbor Scaling)

    - Use (1 / Division Factor) as Prescale Factor

        The big difference here is this new setting. It sets the Prescale Factor to the inverse of whatever the Division Factor is. For example, a Division Factor of 4, would be 1/4, or 0.25. This might sound counterintuitive. If dumbshader and Sharp Bilinear are meant to increase sharpness, and a lower prescale factor means a more blurry image, then what's the point? Well, it all comes down to the simple fact that, normally Sharp Bilinear is only for upscaling. With a division factor, we're instead downscaling, so fractional Prescale Factors are actually useful here.

 - Scanlines

   I'd recommend ignoring this section all together, as it's more of a debug toy than a feature. By default, this is setup for passable 240p scanlines, which is explained below in the "__240p scanlines for any resolution!__" section. As for other configurations, they pretty much require a window size of exactly 1216x961, with Dolphin's aspect ratio set to "Stretch", to achieve a 1:1 PAR window. Getting a PAR window size is unrealistic for most people, so I would strongly recommend against banging your head against the wall with these settings.

    - Scanline Offsets

         This setting does exactly what you'd think for the most part, but there's a small quirk. On certain setting configurations, such as having "Use Solid Line" enabled, it allows non integer offsets (ending in '.5' to halve the line width. This is convenient for having thin scanlines.

    - Use Window Resolution

         by default, the thickness an interval of the scanlines are calculated based off the internal resolution. Having this setting enabled makes the scanlines ignore the resolution entirely, and instead use your window size. This setting overrides the second option, "Scale Line Thickness with Window Size". This has the downside of not syncing up to any pixels unless you're at a 1:1 PAR resolution

    - Scale Line Thickness

        This setting is very similar to the above setting, with one key difference. Instead of being based solely off your window resolution, it instead calculates out how large your window is relative to native resolution, and then uses that to adjust line thickness. What this means that the scanlines will be in the same relative spot to the pixels, which can work with a few-niche non 1:1 PAR scales.


   ![horizontal_scanlines](https://cdn.discordapp.com/attachments/1102669854617649162/1102673087612326049/image.png)
   *Basic 480p Scanlines. Open this image in a new tab, it may make you happy. PAR is 1:1 (window res 1216x961). Divide Resolution off. dumbshader set to default. horizontal scanlines with a width of 2.0, offset of 2.5. 'Solid lines' and 'Scale Line Thickness with Window Size are enabled'. Opacity of 0.66*

   ![lcd_grid](https://cdn.discordapp.com/attachments/1102669854617649162/1102672721105661962/image.png)
   *240p LCD grid. Open this image in a new tab, it may make you happy. PAR is 1:1 (window res 1216x961). Division factor of 2.0. dumbshader set to default. horizontal and vertical scanlines with a width of 4.0, offset of 2.5. 'Solid lines' and 'Scale Line Thickness with Window Size are enabled'. Opacity of 1.0*

Pro tip! Setting Dolphin's internal resolution to x2, and then setting the "Division Factor" in either 'prude_scaler.glsl' or 'crude_division.glsl' to "3.000" will make it 360p! May be desireable when playing N64 ports, like Cubivore or Animal Crossing, since it strikes a balance between chunky low res pixels and textures, and clairity.

### The '240p*' shaders

There are three pre-packaged shaders, which are static derivatives of 'prude_scaler.glsl', all of which are for displaying games at 240p.

 - '240p_crude.glsl'

   Equivalent to 'crude_division' with a target resolution of 240p, and no offsets. Meant to make Animal Crossing look like it's N64 counterpart.

 - '240p_nearest_neighbor_dumbshader.glsl'

   Keeps the game at 240p, but, as the name suggests, uses nearest neighbor scaling for sharp pixels. Paired with Dolphins "Force Nearest" texture filtering setting, this gives things a look reminiscent of the PS1. As mentioned before, this is fiddly with Dolphin's internal resolution, so play around with it! I'd recommend 4x native as a starting point. This is equivalent to using prude_scaler with a target resolution of 240, no offset correction, and dumbshader enabled, with no sharp bilinear filtering.

   ![240p_nearest_neighbor](https://cdn.discordapp.com/attachments/1102669854617649162/1102671599280341092/image.png)
   *nearest neighbor*

 - '240p_sharp_bilinear.glsl' Is similar, but has a small amount of filtering, to help smooth things out. I'd recommend using it either at x1 native resolution, or x4 native. Anything else either has too much filtering, or too much downscaling. This is equivalent to using prude_scaler with a target resolution of 240, always correct offset, dumbshader enabled, sharp bilinear enabled, and division factor as prescale factor.

   ![240p_sharp_bilin](https://cdn.discordapp.com/attachments/1102669854617649162/1102671694419738734/image.png)
   *sharp bilinear*

You may want to play around with Dolphin's "Internal Resolution" while using any of these '240p*' prefixed shaders. This can sharpen how they look significantly, while still being 240p, which may be desired if you want intermediate amounts of aliasing.


## 240p scanlines for any resolution!

There is a way to get acceptable horizontal scanlines for 240p. Not as good as they would be with 1:1 PAR, but still pretty decent. These should be the default settings, but just in case, I'll list the requirements here.

Firstly, you want Dolphin's window size to be roughly x2 native, or x3. Fullscreen should work too.

Then, in 'prude_scaler.glsl's configuration, turn on Divide Resolution, with a division factor of 2 (Or target resolution of 240p). Offsets are optional.

Everything in the dumshader tab is optional, feel free to pick whichever settings you like

In the Scanlines tab, things have to be very specific, so please follow along.

- Enable "Horizontal Scanlines"
- Set "Horizontal Scanline Interval" to 4.0
- Set "Horizontal Scanline Offset" to 2.5
- Disable "Use Solid Line"
- Disable "Use Window Resolution Instead of Internal Resolution"
- Enable "Scale Line Thickness with Window Size"

![the_only_good_one](https://cdn.discordapp.com/attachments/1102669854617649162/1102672377227251802/image.png)
*The only passable horizontal scanline mode without a 1261x961 window*
